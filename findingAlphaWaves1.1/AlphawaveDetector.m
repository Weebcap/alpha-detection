function Result = AlphawaveDetector(controlf,controly,newf,newy)
    
    %datatrim - not necessary for alphaWave functions
    cut = 20; %where to cut from
    fftBaseX = controlf(controlf<cut); %base level noise
    fftBaseY = controly(1:length(fftBaseX));

    cut = 20; %where to cut from
    fftSampleX = newf(newf<cut); %sample data to be tested against base
    fftSampleY = newy(1:length(fftSampleX));

    testmax = alphaWavesMax(fftBaseX, fftBaseY, fftSampleX, fftSampleY); %simple boolean return
    testmean = alphaWavesMean(fftBaseX, fftBaseY, fftSampleX, fftSampleY); %simple boolean return
    
    if (testmax==1 && testmean==1)
        Result = 1;
    else
        Result = 0;
    end

end