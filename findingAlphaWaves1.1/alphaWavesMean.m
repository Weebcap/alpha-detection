%Compares average in 2-20Hz baseline range against the average in the
%8-12Hz sample range and returns true if this average is XX% larger than
%the baseline average AND if it is YY% larger than its own "overall"
%(2-20Hz) average.
function alphaDetect = alphaWavesMean(bX,bY,sX,sY)
    Ab = mean(bY(bX<20 & bX>2));
    As = mean(sY(sX<20 & sX>2));
    AsAlpha = mean(sY(sX<13 & sX>8));
    if AsAlpha > Ab*1.5 % here we have XX% = 50%
       check1 = 1;
    else
       check1 = 0;
    end
    
    if AsAlpha > As*1.2 % here YY% = 20%
        check2 = 1;
    else
        check2 = 0;
    end
    
    if check1*check2 == 1
        alphaDetect = 1;
    else
        alphaDetect = 0;
    end
end