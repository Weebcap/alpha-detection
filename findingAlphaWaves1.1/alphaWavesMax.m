%Compares the max in the 8-12Hz fourier space of two sets of data and
%returns true if sample data peak is XX% larger than the baseline data
function alphaDetect = alphaWavesMax(bX,bY,sX,sY)
    Mb = max(bY(bX<13 & bX>8));
    Ms = max(sY(sX<13 & sX>8));
    if Ms > Mb*1.5 % here we have XX% = 50%
       alphaDetect = 1;
    else
       alphaDetect = 0;
    end
end