
% Function that receives incoming data and takes the FFT
% of a part of it
function [x,y] = EEGFFT(data,Fs,t,St,Ft)
    
    datacut = data(t>=St);
    tcut = t(t>=St);
    datashort = datacut(tcut<Ft);

    Y = fft(datashort);
    F = fftshift(Y);

    dx = 1/Fs;
    L = length(datashort)/Fs;
    T = [1:length(datashort)]/Fs;

    Nq = 0.5/dx;
    f = -Nq : 1/L : Nq-1/L;
    
    % Plotting the FFT
    
    sqmod = abs(F).^2;
    sqmodpos = sqmod(f>0);
    fpos = f(f>0);
%     figure(1);
%     plot(fpos, sqmodpos);
%     xlim([0,20]);
%     xlabel('f'); 
%     ylabel('|F|^2');
    
    x = fpos;
    y = sqmodpos;
end