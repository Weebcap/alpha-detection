clear;
clc;
close all


EEG = load('MLrecord,gel,EEG,open-closed@15,Con,21-03-20.mat');

data = EEG.data_actual;
data = flip(data);
Fs = length(EEG.t)/EEG.max_time;
t = EEG.t;

TWindow = 3;            % observation interval
TMax = max(t);          % maximum time recorded
dT = t(2)-t(1);         % dT
NWindow = TWindow/dT;   % number of points in Window
delay_time = TWindow;   % wait the "recording interval" time

for i = 1:round(TMax/TWindow)
    
    St = TWindow*(i-1);
    Ft = TWindow*i;
    
    window_index_range = St:Ft;
    figure(1)
    clf
    subplot(3,1,1)
    hold all
    plot(t,data,'b-')
    % plot the window you are looking at
    Stplot = round(St*Fs)+1;
    Ftplot = round(Ft*Fs);
    plot([t(Stplot),t(Stplot)],[min(data),max(data)],'k')   
    plot([t(Ftplot),t(Ftplot)],[min(data),max(data)],'k')   
    xlabel('time [s]')
    ylabel('signal [a.u.]')
    box on
    
    if(i==1)
        [controlf,controly] = EEGFFT2(data,Fs,t,St,Ft);
    end
    
    
    [newf,newy] = EEGFFT2(data,Fs,t,St,Ft);
    subplot(3,1,2)
    plot(newf, newy);
    xlim([2,20]);
    xlabel('f'); 
    ylabel('|F|^2');
        

    if(AlphawaveDetector(controlf,controly,newf,newy) == 1)

        subplot(3,1,3)
        xlim([0,10])
        ylim([0,10])
        title('EYES CLOSED','fontsize',16)
        disp('closed')
    else
        subplot(3,1,3)
        xlim([0,10])
        ylim([0,10])
        title('EYES OPEN','fontsize',16)
        disp('open')

    end
    
    pause(TWindow);
end

